// Define a maximum' function. This function is undefined on an empty list.

maximum' :: Ord p => [p] -> p
maximum' (x:xs) = _max x xs where
    _max cmax [] = cmax
    _max cmax (y:ys)
        | y > cmax  = _max y ys
        | otherwise = _max cmax ys
maximum' [] = undefined
